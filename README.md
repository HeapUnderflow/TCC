# TCC

## Example Config

```toml
[twitch]
# Your login oauth token
oauth = "oauth:XXXXXXXXXXXXXXXXXXXXXXXXXXX"
# your UNCAPITALIZED twitch user name
username = "example_username"
# the channels you want to joint on start (prefixed with #)
# example: channel = ["#cha", "#chb"]
channel = []
# the words you want to highlight on
# (higlight will only trigger when the word is surrounded by 2 spaces in the twich message)
highlight = []

# twitch connection info, can be left as-is
[twitch.connection]
server = "irc.chat.twitch.tv"
port = 6667
ssl = false

[twitch.optout]
join = true
leave = true
```

## Environment Overrides

Most config options can be overloaded by specifying a corresponding environment variable

| Config Option     | Environment Name     |
| ----------------- | -------------------- |
| oauth             | `TCC_OAUTH`          |
| username          | `TCC_USERNAME`       |
| channel           | `TCC_CHAT_JOIN`      |
| highlight         | `TCC_CHAT_HIGHLIGHT` |
| connection.server | `TCC_CONN_SERVER`    |
| connection.port   | `TCC_CONN_PORT`      |
| connection.ssl    | `TCC_CONN_SSL`       |