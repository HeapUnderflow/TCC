use chrono::Local;
use color;
use irc::proto::message::Tag;
use std::{collections::HashMap, iter::FromIterator};

#[inline(always)]
#[allow(dead_code)]
pub fn opt_string(s: Option<String>, default: String) -> String {
    if let Some(x) = s {
        x
    } else {
        default
    }
}

#[inline(always)]
#[allow(dead_code)]
pub fn opt_string_apply<T>(s: Option<String>, default: String, on_ok: T) -> String
where
    T: FnOnce(String) -> String,
{
    if let Some(x) = s {
        on_ok(x)
    } else {
        default
    }
}

#[inline(always)]
#[allow(dead_code)]
pub fn bool_string(s: bool, yes: String, no: String) -> String {
    if s {
        yes
    } else {
        no
    }
}

#[allow(dead_code)]
pub fn f_dump_s(fx: &str, s: &str) -> ::std::io::Result<()> {
    use std::{fs::File, io::Write};

    let mut f = File::create(fx)?;
    write!(f, "{}", s)?;

    Ok(())
}

#[inline]
#[allow(dead_code)]
pub fn find_twitch_name_prefx(s: Option<String>) -> String {
    if let Some(prefx) = s {
        let s = prefx.split('@').collect::<Vec<_>>();
        if s.len() == 2 {
            let sx = s[0].split('!').collect::<Vec<_>>();
            if sx.len() == 2 {
                String::from(sx[0])
            } else {
                String::from(s[0])
            }
        } else {
            String::from("[SOMEBODY]")
        }
    } else {
        String::from("[UNKNOWN]")
    }
}

/// Abbreviation function for `String::from(o)`
#[inline(always)]
#[allow(dead_code)]
pub fn s(o: &str) -> String { String::from(o) }

/// Abbreviation function for `model::color::ColoredString::to_string()`
#[inline(always)]
#[allow(dead_code)]
pub fn sc(o: &color::ColoredString) -> String { o.to_string() }

pub fn stringy_bool(s: &str) -> bool {
    match s {
        "y" | "yes" | "1" | "true" => true,
        _ => false,
    }
}

// This is *NOT* alloc free
pub fn tags_into_hashmap(tags: &[Tag]) -> HashMap<String, String> {
    HashMap::from_iter(
        tags.into_iter()
            .filter(|i| i.1 != None && i.1 != Some("".to_owned()))
            .map(|Tag(l, r)| (l.clone(), r.clone().unwrap())),
    )
}

pub fn localtime() -> String {
    let local = Local::now();
    format!("{}", local.format("%T"))
}
