use std::{fmt, ops::Deref};
use tui::style::{Color, Modifier, Style};

#[derive(Clone, Debug, PartialEq)]
pub struct ColoredString {
    input: String,
    style: Style,
}

/// The trait that enables something to be given color.
///
/// You can use `colored` effectively simply by importing this trait
/// and then using its methods on `String` and `&str`.
// This `trait` was copied from https://github.com/mackwic/colored/blob/0b4ce98dacdddf1b908c4f6c782a97526d2b4e34/src/lib.rs#L53-L113
// Documentation was preserved.
pub trait Colorize {
    // Font Colors
    fn black(self) -> ColoredString;
    fn red(self) -> ColoredString;
    fn green(self) -> ColoredString;
    fn yellow(self) -> ColoredString;
    fn blue(self) -> ColoredString;
    fn magenta(self) -> ColoredString;
    fn purple(self) -> ColoredString;
    fn cyan(self) -> ColoredString;
    fn white(self) -> ColoredString;
    fn bright_black(self) -> ColoredString;
    fn bright_red(self) -> ColoredString;
    fn bright_green(self) -> ColoredString;
    fn bright_yellow(self) -> ColoredString;
    fn bright_blue(self) -> ColoredString;
    fn bright_magenta(self) -> ColoredString;
    fn bright_purple(self) -> ColoredString;
    fn bright_cyan(self) -> ColoredString;
    fn bright_white(self) -> ColoredString;
    fn color<S: Into<Color>>(self, color: S) -> ColoredString;
    // Background Colors
    fn on_black(self) -> ColoredString;
    fn on_red(self) -> ColoredString;
    fn on_green(self) -> ColoredString;
    fn on_yellow(self) -> ColoredString;
    fn on_blue(self) -> ColoredString;
    fn on_magenta(self) -> ColoredString;
    fn on_purple(self) -> ColoredString;
    fn on_cyan(self) -> ColoredString;
    fn on_white(self) -> ColoredString;
    fn on_bright_black(self) -> ColoredString;
    fn on_bright_red(self) -> ColoredString;
    fn on_bright_green(self) -> ColoredString;
    fn on_bright_yellow(self) -> ColoredString;
    fn on_bright_blue(self) -> ColoredString;
    fn on_bright_magenta(self) -> ColoredString;
    fn on_bright_purple(self) -> ColoredString;
    fn on_bright_cyan(self) -> ColoredString;
    fn on_bright_white(self) -> ColoredString;
    fn on_color<S: Into<Color>>(self, color: S) -> ColoredString;
    // Styles
    fn clear(self) -> ColoredString;
    fn normal(self) -> ColoredString;
    fn bold(self) -> ColoredString;
    fn dimmed(self) -> ColoredString;
    fn italic(self) -> ColoredString;
    fn underline(self) -> ColoredString;
    fn blink(self) -> ColoredString;
    /// Historical name of `Colorize::reversed`. May be removed in a future
    /// version. Please use `Colorize::reversed` instead
    fn reverse(self) -> ColoredString;
    /// This should be preferred to `Colorize::reverse`.
    fn reversed(self) -> ColoredString;
    fn hidden(self) -> ColoredString;
    fn strikethrough(self) -> ColoredString;
}

impl ColoredString {
    fn has_colors(&self) -> bool {
        self.style.fg != Color::Reset
            || self.style.bg != Color::Reset
            || self.style.modifier != Modifier::Reset
    }

    fn is_plain(&self) -> bool {
        self.style.fg == Color::White
            && self.style.bg == Color::Reset
            && self.style.modifier == Modifier::Reset
    }

    fn compute_style(&self) -> String {
        let mut dirty = false;
        let mut s = String::new();
        if self.style.fg != Color::Reset {
            s.push_str(&format!("fg={}", color_to_str(self.style.fg)));
            dirty = true;
        }

        if self.style.bg != Color::Reset {
            if dirty {
                s.push(';');
            }
            s.push_str(&format!("bg={}", color_to_str(self.style.bg)));
        }

        if self.style.modifier != Modifier::Reset {
            if dirty {
                s.push(';');
            }
            s.push_str(&format!("mod={}", modifier_to_str(self.style.modifier)));
        }
        s
    }

    fn escape_input(&self) -> String {
        self.input
            .replace("\\", "\\\\")
            .replace("{", "\\{")
            .replace("}", "\\}")
            .replace("[", "\\[")
            .replace("]", "\\]")
            .replace("(", "\\(")
            .replace(")", "\\)")
    }
}

impl Default for ColoredString {
    fn default() -> ColoredString {
        ColoredString {
            input: String::default(),
            style: Style::default(),
        }
    }
}

impl Deref for ColoredString {
    type Target = str;

    fn deref(&self) -> &str { &self.input }
}

impl<'a> From<&'a str> for ColoredString {
    fn from(s: &'a str) -> Self {
        ColoredString {
            input: String::from(s),
            style: Style::default(),
        }
    }
}

macro_rules! def_color {
    ($side:ident: $name:ident => $color:path) => {
        fn $name(self) -> ColoredString {
            self.style.$side($color);
            self
        }
    }
}

macro_rules! def_style {
    ($name:ident, $value:path) => {
        fn $name(self) -> ColoredString {
            self.style.modifier($value);
            self
        }
    }
}

macro_rules! def_unimplemented {
    ($name:ident) => {
        fn $name(self) -> ColoredString {
            unimplemented!();
        }
    };
}

#[cfg_attr(rustftm, rustfmt_skip)]
impl Colorize for ColoredString {
    def_unimplemented!(hidden);
    def_unimplemented!(reversed);
    def_unimplemented!(reverse);
    def_style!(strikethrough,           Modifier::CrossedOut);
    def_style!(blink,                   Modifier::Blink);
    def_style!(underline,               Modifier::Underline);
    def_style!(italic,                  Modifier::Italic);
    def_style!(dimmed,                  Modifier::Faint);
    def_style!(bold,                    Modifier::Bold);
    def_color!(bg: on_bright_white   => Color::White);
    def_color!(bg: on_bright_cyan    => Color::LightCyan);
    def_color!(bg: on_bright_purple  => Color::LightMagenta);
    def_color!(bg: on_bright_magenta => Color::LightMagenta);
    def_color!(bg: on_bright_blue    => Color::LightBlue);
    def_color!(bg: on_bright_yellow  => Color::LightYellow);
    def_color!(bg: on_bright_green   => Color::LightGreen);
    def_color!(bg: on_bright_red     => Color::LightRed);
    def_color!(bg: on_bright_black   => Color::Gray);
    def_color!(bg: on_white          => Color::White);
    def_color!(bg: on_cyan           => Color::Cyan);
    def_color!(bg: on_purple         => Color::Magenta);
    def_color!(bg: on_magenta        => Color::Magenta);
    def_color!(bg: on_blue           => Color::Blue);
    def_color!(bg: on_yellow         => Color::Yellow);
    def_color!(bg: on_green          => Color::Green);
    def_color!(bg: on_red            => Color::Red);
    def_color!(bg: on_black          => Color::Black);
    def_color!(fg: bright_white      => Color::White);
    def_color!(fg: bright_cyan       => Color::LightCyan);
    def_color!(fg: bright_purple     => Color::LightMagenta);
    def_color!(fg: bright_magenta    => Color::LightMagenta);
    def_color!(fg: bright_blue       => Color::LightBlue);
    def_color!(fg: bright_yellow     => Color::LightYellow);
    def_color!(fg: bright_green      => Color::LightGreen);
    def_color!(fg: bright_red        => Color::LightRed);
    def_color!(fg: bright_black      => Color::DarkGray);
    def_color!(fg: white             => Color::White);
    def_color!(fg: cyan              => Color::Cyan);
    def_color!(fg: purple            => Color::Magenta);
    def_color!(fg: magenta           => Color::Magenta);
    def_color!(fg: blue              => Color::Blue);
    def_color!(fg: yellow            => Color::Yellow);
    def_color!(fg: green             => Color::Green);
    def_color!(fg: red               => Color::Red);
    def_color!(fg: black             => Color::Black);

    fn color<S: Into<Color>>(self, color: S) -> ColoredString {
        self.style.fg(color.into());
        self
    }

    fn on_color<S: Into<Color>>(self, color: S) -> ColoredString {
        self.style.bg(color.into());
        self
    }

    fn clear(self) -> ColoredString {
        ColoredString {
            input: self.input,
            style: Style::default(),
        }
    }

    fn normal(self) -> ColoredString { self.clear() }
}

macro_rules! def_str_color {
    ($side:ident: $name:ident => $color:path) => {
        fn $name(self) -> ColoredString {
            ColoredString {
                input: String::from(self),
                style: Style::default().$side($color)
            }
        }
    }
}

macro_rules! def_str_style {
    ($name:ident, $value:path) => {
        fn $name(self) -> ColoredString {
            ColoredString {
                input: String::from(self),
                style: Style::default().modifier($value)
            }
        }
    }
}

#[cfg_attr(rustftm, rustfmt_skip)]
impl<'a> Colorize for &'a str {
    def_unimplemented!(hidden);
    def_unimplemented!(reversed);
    def_unimplemented!(reverse);
    def_str_style!(strikethrough,           Modifier::CrossedOut);
    def_str_style!(blink,                   Modifier::Blink);
    def_str_style!(underline,               Modifier::Underline);
    def_str_style!(italic,                  Modifier::Italic);
    def_str_style!(dimmed,                  Modifier::Faint);
    def_str_style!(bold,                    Modifier::Bold);
    def_str_color!(bg: on_bright_white   => Color::White);
    def_str_color!(bg: on_bright_cyan    => Color::LightCyan);
    def_str_color!(bg: on_bright_purple  => Color::LightMagenta);
    def_str_color!(bg: on_bright_magenta => Color::LightMagenta);
    def_str_color!(bg: on_bright_blue    => Color::LightBlue);
    def_str_color!(bg: on_bright_yellow  => Color::LightYellow);
    def_str_color!(bg: on_bright_green   => Color::LightGreen);
    def_str_color!(bg: on_bright_red     => Color::LightRed);
    def_str_color!(bg: on_bright_black   => Color::Gray);
    def_str_color!(bg: on_white          => Color::White);
    def_str_color!(bg: on_cyan           => Color::Cyan);
    def_str_color!(bg: on_purple         => Color::Magenta);
    def_str_color!(bg: on_magenta        => Color::Magenta);
    def_str_color!(bg: on_blue           => Color::Blue);
    def_str_color!(bg: on_yellow         => Color::Yellow);
    def_str_color!(bg: on_green          => Color::Green);
    def_str_color!(bg: on_red            => Color::Red);
    def_str_color!(bg: on_black          => Color::Black);
    def_str_color!(fg: bright_white      => Color::White);
    def_str_color!(fg: bright_cyan       => Color::LightCyan);
    def_str_color!(fg: bright_purple     => Color::LightMagenta);
    def_str_color!(fg: bright_magenta    => Color::LightMagenta);
    def_str_color!(fg: bright_blue       => Color::LightBlue);
    def_str_color!(fg: bright_yellow     => Color::LightYellow);
    def_str_color!(fg: bright_green      => Color::LightGreen);
    def_str_color!(fg: bright_red        => Color::LightRed);
    def_str_color!(fg: bright_black      => Color::DarkGray);
    def_str_color!(fg: white             => Color::White);
    def_str_color!(fg: cyan              => Color::Cyan);
    def_str_color!(fg: purple            => Color::Magenta);
    def_str_color!(fg: magenta           => Color::Magenta);
    def_str_color!(fg: blue              => Color::Blue);
    def_str_color!(fg: yellow            => Color::Yellow);
    def_str_color!(fg: green             => Color::Green);
    def_str_color!(fg: red               => Color::Red);
    def_str_color!(fg: black             => Color::Black);

    fn color<S: Into<Color>>(self, color: S) -> ColoredString {
            ColoredString {
                input: String::from(self),
                style: Style::default().fg(color.into())
            }
    }

    fn on_color<S: Into<Color>>(self, color: S) -> ColoredString {
        ColoredString {
                input: String::from(self),
                style: Style::default().bg(color.into())
            }
    }

    fn clear(self) -> ColoredString {
        ColoredString {
            input: String::from(self),
            style: Style::default().fg(Color::Reset).bg(Color::Reset).modifier(Modifier::Reset),
        }
    }

    fn normal(self) -> ColoredString { self.clear() }
}

impl fmt::Display for ColoredString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if !self.has_colors() || self.is_plain() {
            return f.write_str(&self.input);
        }

        let sty = self.compute_style();
        if sty.is_empty() {
            f.write_str(&self.input)?;
        } else {
            let inp = self.escape_input();

            f.write_str("{")?;
            f.write_str(&format!("{} {}", sty, inp))?;
            f.write_str("}")?;
        }
        Ok(())
    }
}

fn color_to_str(c: Color) -> &'static str {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    match c {
        Color::Black        => "black",
        Color::Red          => "red",
        Color::Green        => "green",
        Color::Yellow       => "yellow",
        Color::Blue         => "blue",
        Color::Magenta      => "magenta",
        Color::Cyan         => "cyan",
        Color::Gray         => "gray",
        Color::DarkGray     => "dark_gray",
        Color::LightRed     => "light_red",
        Color::LightGreen   => "light_green",
        Color::LightBlue    => "light_blue",
        Color::LightYellow  => "light_yellow",
        Color::LightMagenta => "light_magenta",
        Color::LightCyan    => "light_cyan",
               _            => "",
    }
}

fn modifier_to_str(m: Modifier) -> &'static str {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    match m {
        Modifier::Bold       => "bold",
        Modifier::Italic     => "italic",
        Modifier::Underline  => "underline",
        Modifier::Invert     => "invert",
        Modifier::CrossedOut => "crossed_out",
                  _          => "",
    }
}
