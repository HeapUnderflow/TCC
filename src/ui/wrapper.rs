use std::mem;
use textwrap;

#[derive(Debug, Clone)]
/// A word wrapper.
/// Takes input lines like a vector (with [WordWrapper::push]).
/// The wrapped text (to the specified width) is returned with [WordWrapper::get_text].
/// The text is only rerendered, when the content was modified or marked dirty with [WordWrapper:mark_dirty]
pub struct WordWrapper {
    screen_lines: Vec<String>,
    dirty:        bool,
    input_lines:  Vec<String>,
    termwidth:    usize,
}

impl Default for WordWrapper {
    fn default() -> WordWrapper {
        WordWrapper {
            screen_lines: Vec::new(),
            dirty:        true,
            input_lines:  Vec::new(),
            termwidth:    80,
        }
    }
}

impl WordWrapper {
    /// Create a new instance of this object with the given wrap width.
    pub fn new(width: usize) -> Self {
        WordWrapper {
            screen_lines: Vec::new(),
            dirty:        true,
            input_lines:  Vec::new(),
            termwidth:    width,
        }
    }

    /// Returns the amount of the rendered lines.
    /// This does **not** regenerate them if the input has been modified.
    pub fn len(&self) -> usize { self.screen_lines.len() }

    /// Returns the amount of the input lines.
    pub fn input_len(&self) -> usize { self.input_lines.len() }

    /// Sets the width to render for.
    /// Forces regeneration on next access.
    pub fn set_width(&mut self, w: usize) {
        self.termwidth = w;
        self.dirty = true;
    }

    /// Get the width that is rendered for.
    pub fn get_width(&mut self) -> usize { self.termwidth }

    /// Add a new line to the line list
    pub fn push(&mut self, s: String) {
        self.input_lines.push(s);
        self.dirty = true;
    }

    /// Forces the buffer to be regenerated, no matter what.
    pub fn mark_dirty(&mut self) { self.dirty = true; }

    fn check_dirty(&mut self) {
        if self.dirty {
            if self.input_lines.len() == 0 {
                let _ = mem::replace(&mut self.screen_lines, Vec::new());
            } else {
                let mut nvec: Vec<String> = Vec::with_capacity(self.input_lines.len());
                let wrapper = textwrap::Wrapper::new(self.termwidth);
                for line in &self.input_lines {
                    for wrap in wrapper.wrap_iter(line) {
                        nvec.push(wrap.to_string());
                    }
                }
                let _ = mem::replace(&mut self.screen_lines, nvec);
            }
            self.dirty = false;
        }
    }

    /// Return a "Window" of lines items with an offset of scroll.
    /// This is seen from the end
    /// 
    /// # Example
    /// ```rust
    /// let mut textwrap = WordWrapper::new(10);
    /// textwrap.push("Hello World!");
    /// textwrap.push("I am here!");
    /// textwrap.push("YES");
    /// 
    /// let text = textwrap.get_text(2, 1);
    /// # let check = vec!["I am here!", "World!"].iter().map(|s| String::from(s)).collect<Vec<String>>();
    /// assert_eq!(Ok(&check), text);
    /// ```
    pub fn get_text(&mut self, lines: u16, scroll: u16) -> Result<&[String], String> {
        self.check_dirty();
        // lines = amount of lines requested
        // scroll = offset

        if lines == 0 || self.screen_lines.len() == 0 {
            Ok(&[]) // We do not have data
        } else if scroll == 0 {
            // User just requests some lines with no offset
            if lines > self.screen_lines.len() as u16 {
                // Not a failure as you can "request" x lines, but not have them avaliable
                Ok(&self.screen_lines[..])
            } else {
                Ok(&self.screen_lines[self.screen_lines.len() - lines as usize..])
            }
        } else {
            if scroll >= self.screen_lines.len() as u16 {
                Err(String::from("Cannot scroll past end"))
            } else {
                let to = self.screen_lines.len() - scroll as usize;
                let from = match to.checked_sub(lines as usize) {
                    Some(off) => off,
                    None => 0,
                };

                Ok(&self.screen_lines[from..to])
            }
        }
    }
}
