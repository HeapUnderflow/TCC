use irc::proto::Command;

pub fn hl_sx(s: &str, at: usize) -> String {
    if s.is_empty() {
        return String::new();
    }

    if s.len() == 1 {
        return format!("{{mod=invert {}}}", s);
    }

    let (l, r) = s.split_at(at);
    let mut n = String::with_capacity(s.len() + "{mod=invert X}".len());
    n.push_str(l);
    n.push_str(&format!("{{mod=invert {}}}", &r[..1]));
    n.push_str(&r[1..]);
    n
}

pub fn prepare_message(chn: &str, msg: &str) -> Command {
    let ch = if !chn.starts_with("#") {
        format!("#{}", chn)
    } else {
        String::from(chn)
    };
    Command::PRIVMSG(ch, String::from(msg))
}

pub fn prepare_join(chn: &str) -> Command {
    let ch = if !chn.starts_with("#") {
        format!("#{}", chn)
    } else {
        String::from(chn)
    };
    Command::JOIN(ch, None, None)
}

pub fn prepare_part(chn: &str) -> Command {
    let ch = if !chn.starts_with("#") {
        format!("#{}", chn)
    } else {
        String::from(chn)
    };
    Command::PART(ch, None)
}
