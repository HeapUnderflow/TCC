// extern crate rand;
// extern crate termion;
// extern crate tui;

use color::Colorize;
use irc::{
    client::{Client, IrcClient},
    proto::Command,
};
use std::{
    collections::HashMap,
    io,
    sync::{
        atomic::{AtomicBool, Ordering as AtomicOrdering},
        mpsc::{channel, Receiver, Sender},
        Arc,
        Barrier,
        BarrierWaitResult,
    },
    thread::{self, JoinHandle},
    time,
};
use termion::{event, input::TermRead};
use tui::{
    backend::MouseBackend,
    layout::{Direction, Group, Rect, Size},
    widgets::{Block, Borders, Paragraph, Widget},
    Terminal,
};

pub mod utils;
pub mod wrapper;

#[derive(Debug, PartialEq)]
pub enum Event {
    Input(event::Key),
    Data(String),
    IrcCommand(Command),
    UserState(String, String),
    Tick,
}

#[derive(Debug, Clone)]
pub struct State {
    size:             Rect,
    lines:            wrapper::WordWrapper,
    input:            Vec<char>,
    cursor_leftset:   usize,
    user_state_cache: HashMap<String, String>,
    selected_channel: String,
}

pub struct UI {
    join_handle: JoinHandle<()>,
    _cancle:     Arc<AtomicBool>,
    barrier:     Arc<Barrier>,
}

impl UI {
    pub fn new(client: IrcClient, default_channel: String) -> io::Result<(UI, Sender<Event>)> {
        let cncl = Arc::new(AtomicBool::new(false));
        let thread_cncl = Arc::clone(&cncl);

        let (tx, rx) = channel::<Event>();

        let thread_tx = tx.clone();

        let barrier = Arc::new(Barrier::new(2));
        let thbr = barrier.clone();

        Ok((
            UI {
                join_handle: thread::Builder::new().name("UI Thread".to_owned()).spawn(
                    move || {
                        let _cancle = thread_cncl;
                        let rx = rx;
                        let tx = thread_tx;
                        let br = thbr;

                        thread_loop(_cancle, rx, tx, br, default_channel, client).unwrap();
                    },
                )?,
                _cancle: cncl,
                barrier,
            },
            tx,
        ))
    }

    pub fn wait_on_init(&self) -> BarrierWaitResult { self.barrier.wait() }

    pub fn stop(self) {
        self._cancle.store(true, AtomicOrdering::Relaxed);
        self.join_handle.join().unwrap();
    }

    pub fn get_stop(&self) -> Arc<AtomicBool> { Arc::clone(&self._cancle) }
}

fn thread_loop(
    _cancle: Arc<AtomicBool>,
    rx: Receiver<Event>,
    tx: Sender<Event>,
    br: Arc<Barrier>,
    default_channel: String,
    client: IrcClient,
) -> io::Result<()> {
    let backend = MouseBackend::new()?;
    let mut terminal = Terminal::new(backend)?;

    let mut state = State {
        size:             Rect::default(),
        lines:            wrapper::WordWrapper::default(),
        input:            Vec::new(),
        cursor_leftset:   0,
        user_state_cache: HashMap::new(),
        selected_channel: default_channel,
    };

    start_input_loop(tx.clone())?;
    start_tick_loop(tx.clone())?;
    let out = start_output_loop(client)?;

    terminal.clear().expect("Unable to clear the screen");
    terminal.hide_cursor()?;
    draw(&mut terminal, &mut state).expect("Failed to draw");

    br.wait();

    'wrkloop: loop {
        let size = terminal.size()?;
        if size != state.size {
            terminal.resize(size)?;
            state.size = size;
        }

        let evt = match rx.recv() {
            Ok(ev) => ev,
            Err(_) => break 'wrkloop,
        };

        match evt {
            Event::Input(input) => {
                match input {
                    event::Key::Char('\n') => {
                        if &state.input == &['$', 'q', 'u', 'i', 't'] {
                            _cancle.store(true, AtomicOrdering::Relaxed);
                            match out.send(Event::IrcCommand(Command::PING(
                                String::from("tmi.twitch.tv"),
                                None,
                            ))) {
                                Ok(_) => (),
                                Err(_) => (),
                            }
                            break 'wrkloop;
                        }

                        let data = state.input.iter().collect::<String>();

                        if data.starts_with("$select") {
                            let sp = data.split(' ').collect::<Vec<_>>();
                            if sp.len() >= 2 {
                                let ch = match sp[1] {
                                    c if c.starts_with("#") => c.to_owned(),
                                    c => format!("#{}", c),
                                };

                                if state.user_state_cache.contains_key(&ch) {
                                    state.selected_channel = ch;
                                } else {
                                    state.lines.push(format!(
                                        "{} {}",
                                        "# Command Error, invalid channel:".bright_red(),
                                        ch.yellow()
                                    ));
                                }
                            } else {
                                state.lines.push(format!(
                                    "{} {}",
                                    "# Command Error, usage:".bright_red(),
                                    "$select [#]channel".yellow()
                                ));
                            }
                        } else if data.starts_with("$join") {
                            let sp = data.split(' ').collect::<Vec<_>>();
                            if sp.len() >= 2 {
                                let ch = match sp[1] {
                                    c if c.starts_with("#") => c.to_owned(),
                                    c => format!("#{}", c),
                                };

                                let cmm = utils::prepare_join(&ch);
                                match out.send(Event::IrcCommand(cmm)) {
                                    Ok(_) => (),
                                    Err(_) => break 'wrkloop,
                                }

                                state.lines.push(format!(
                                    "{} {}",
                                    "# Joining".bright_yellow(),
                                    ch.yellow()
                                ));
                            } else {
                                state.lines.push(format!(
                                    "{} {}",
                                    "# Command Error, usage:".bright_red(),
                                    "$join [#]channel".yellow()
                                ));
                            }
                        } else if data.starts_with("$leave") || data.starts_with("$part") {
                            let sp = data.split(' ').collect::<Vec<_>>();
                            if sp.len() >= 2 {
                                let ch = match sp[1] {
                                    c if c.starts_with("#") => c.to_owned(),
                                    c => format!("#{}", c),
                                };

                                let cmm = utils::prepare_part(&ch);
                                match out.send(Event::IrcCommand(cmm)) {
                                    Ok(_) => (),
                                    Err(_) => break 'wrkloop,
                                }

                                state.lines.push(format!(
                                    "{} {}",
                                    "# Leaving".bright_yellow(),
                                    ch.yellow()
                                ));
                                match state.user_state_cache.remove(&ch) {
                                    Some(_) => state.lines.push(format!(
                                        "{} {}",
                                        "# Left".bright_yellow(),
                                        ch.bright_green()
                                    )),
                                    None => state.lines.push(format!(
                                        "{} {}",
                                        "# We never where in channel".bright_yellow(),
                                        ch.bright_red()
                                    )),
                                }
                            } else {
                                state.lines.push(format!(
                                    "{} {}",
                                    "# Command Error, usage:".bright_red(),
                                    "$join [#]channel".yellow()
                                ));
                            }
                        } else {
                            let comm = utils::prepare_message(&state.selected_channel, &data);
                            match out.send(Event::IrcCommand(comm)) {
                                Ok(_) => (),
                                Err(_) => break 'wrkloop,
                            }
                            let _def_err = String::from("INVALID_CHANNEL_FORMATS [Missing]");
                            state.lines.push(format!(
                                "[{}]{}: {}",
                                ::utils::localtime(),
                                state
                                    .user_state_cache
                                    .get(&state.selected_channel)
                                    .unwrap_or_else(|| &_def_err),
                                data
                            ));
                        }
                        state.input.clear();
                    },

                    event::Key::Up => {
                        // TODO: Add Scrolling
                    },
                    event::Key::Down => {
                        // TODO: Add Scrolling
                    },
                    event::Key::Left => {
                        state.cursor_leftset += 1;
                    },
                    event::Key::Right => {
                        if state.cursor_leftset > 0 {
                            state.cursor_leftset -= 1;
                        }
                    },
                    event::Key::Backspace => {
                        if state.cursor_leftset > 0 {
                            let ilen = state.input.len();
                            if let Some(p) = (ilen - state.cursor_leftset).checked_sub(1) {
                                state.input.remove(p);
                            }
                        } else {
                            state.input.pop();
                        }
                    },
                    event::Key::Char(c) => {
                        if state.cursor_leftset > 0 {
                            let ilen = state.input.len();
                            state.input.insert(ilen - state.cursor_leftset, c);
                        } else {
                            state.input.push(c)
                        };
                    },
                    _ => (),
                }
            },
            Event::Data(d) => {
                state.lines.push(d);
            },
            Event::UserState(chn, fmt) => {
                // TODO: Add own ON_MESSAGE time (instead that of the last
                // message)
                state
                    .user_state_cache
                    .entry(chn)
                    .and_modify(|e| *e = fmt.clone())
                    .or_insert(fmt);
            },
            _ => (),
        }
        draw(&mut terminal, &mut state)?;
    }
    _cancle.compare_and_swap(false, true, AtomicOrdering::Relaxed);
    terminal.clear()?;
    terminal.show_cursor()?;
    Ok(())
}

fn start_input_loop(tx: Sender<Event>) -> Result<JoinHandle<()>, io::Error> {
    thread::Builder::new()
        .name("UI Input Loop".to_owned())
        .spawn(move || {
            let stdin = io::stdin();
            for c in stdin.keys() {
                let evt = c.unwrap();
                match tx.send(Event::Input(evt)) {
                    Ok(_) => (),
                    Err(_) => return,
                }
            }
        })
}

fn start_tick_loop(tx: Sender<Event>) -> Result<JoinHandle<()>, io::Error> {
    thread::Builder::new()
        .name("UI Tick Loop".to_owned())
        .spawn(move || {
            match tx.send(Event::Tick) {
                Ok(_) => (),
                Err(_) => return,
            }
            thread::sleep(time::Duration::from_millis(500));
        })
}

fn start_output_loop(client: IrcClient) -> io::Result<Sender<Event>> {
    let (tx, rx) = channel();
    thread::Builder::new()
        .name("UI Output Loop".to_owned())
        .spawn(move || loop {
            let d = match rx.recv() {
                Ok(o) => o,
                Err(_) => return,
            };
            match d {
                Event::IrcCommand(comm) => match client.send(comm) {
                    Ok(_) => (),
                    Err(_) => (),
                },
                _ => (),
            }
        })?;
    Ok(tx)
}

fn draw(t: &mut Terminal<MouseBackend>, state: &mut State) -> Result<(), io::Error> {
    let size = t.size()?;

    Group::default()
        .direction(Direction::Vertical)
        .sizes(&[Size::Percent(90), Size::Min(1)])
        .render(t, &size, |t, chunks| {
            if chunks[0].width != state.lines.get_width() as u16 {
                state.lines.set_width(chunks[0].width as usize);
            }

            let text = match state.lines.get_text(chunks[0].height.checked_sub(1).unwrap_or(0), 0) {
                Ok(o) => o.join("\n"),
                Err(e) => format!("ERROR RENDERING TEXT !\n{}", e),
            };

            Paragraph::default()
                .block(Block::default().borders(Borders::ALL))
                .text(&text)
                .render(t, &chunks[0]);

            let sl = format!("{} ", state.input.iter().collect::<String>());
            let selec = format!(" Selected: {} ", state.selected_channel);
            Paragraph::default()
                .block(Block::default().borders(Borders::ALL).title(&selec))
                .text(&utils::hl_sx(
                    &sl,
                    (sl.len() - state.cursor_leftset)
                        .checked_sub(1)
                        .unwrap_or(0),
                ))
                .wrap(true)
                .render(t, &chunks[1]);
        });

    t.draw()
}
