use std::{
    collections::HashMap,
    env,
    fs::{self, File},
    io::{self, Write},
    path::Path,
};
use toml;

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct Config {
    pub twitch: TwitchConfig,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct TwitchConfig {
    pub oauth:      String,
    pub username:   String,
    pub channel:    Vec<String>,
    pub highlight:  Vec<String>,
    pub connection: TwitchConnectionConfig,
    pub optout:     HashMap<String, bool>,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct TwitchConnectionConfig {
    pub server: String,
    pub port:   u16,
    pub ssl:    bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            twitch: TwitchConfig::default(),
        }
    }
}

impl Default for TwitchConfig {
    fn default() -> Self {
        Self {
            oauth:      String::from("oauth:XXXXXXXXXXXXXXXXXXXXXXXXXXX"),
            username:   String::from("example_username"),
            channel:    Vec::new(),
            highlight:  Vec::new(),
            connection: TwitchConnectionConfig::default(),
            optout:     HashMap::default(),
        }
    }
}

impl Default for TwitchConnectionConfig {
    fn default() -> Self {
        Self {
            server: String::from("irc.chat.twitch.tv"),
            port:   6667,
            ssl:    false,
        }
    }
}

pub fn load(path: &str) -> io::Result<Config> {
    if !Path::new(path).exists() {
        let newcfg = Config::default();
        let scfg =
            toml::to_string(&newcfg).expect("The programmer fucked up the config... hard...");

        let mut f = File::create(path)?;
        write!(f, "{}", scfg)?;
        return Err(io::Error::new(io::ErrorKind::Other, "Missing config file"));
    }

    let data = fs::read_to_string(path)?;
    match toml::from_str::<Config>(&data) {
        Ok(mut o) => {
            if let Ok(var) = env::var("TCC_OAUTH") {
                o.twitch.oauth = var;
            }

            if let Ok(var) = env::var("TCC_USERNAME") {
                o.twitch.username = var;
            }

            if let Ok(var) = env::var("TCC_CONN_SERVER") {
                o.twitch.connection.server = var;
            }

            if let Ok(var) = env::var("TCC_CONN_PORT") {
                match var.parse::<u16>() {
                    Ok(port) => o.twitch.connection.port = port,
                    Err(e) => return Err(io::Error::new(io::ErrorKind::InvalidInput, e)),
                }
            }

            if let Ok(var) = env::var("TCC_CHAT_HIGHLIGHT") {
                let parts = var
                    .split(',')
                    .map(|i| i.to_owned())
                    .collect::<Vec<String>>();
                o.twitch.highlight = parts;
            }

            if let Ok(var) = env::var("TCC_CHAT_JOIN") {
                let parts = var
                    .split(',')
                    .map(|i| i.to_owned())
                    .collect::<Vec<String>>();
                o.twitch.channel = parts;
            }

            if let Ok(var) = env::var("TCC_CONN_SSL") {
                o.twitch.connection.ssl = ::utils::stringy_bool(&var);
            }

            o.twitch.highlight.push(format!("@{}", o.twitch.username));

            Ok(o)
        },
        Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
    }
}
