extern crate chrono;
extern crate colored;
extern crate dotenv;
extern crate irc;
extern crate parking_lot;
extern crate regex;
extern crate serde;
extern crate termion;
extern crate textwrap;
extern crate toml;
extern crate tui;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate lazy_static;

pub mod config;
#[macro_use]
pub mod utils;
pub mod color;
#[macro_use]
pub mod client;
pub mod ui;

use config::Config;
use irc::{
    client::prelude::{Client, Command, Config as IrcConfig, IrcReactor},
    proto::command::CapSubCommand,
};

lazy_static! {
    pub static ref PERSISTENT_CONFIG: Config =
        config::load("tccconf.toml").expect("Unable to load Config");
}

fn main() -> Result<(), ::client::Error> {
    dotenv::dotenv().ok();
    let channels = &PERSISTENT_CONFIG.twitch.channel;

    // Creating the reactor (event-loop) and initializing it with the config

    let mut reactor = IrcReactor::new().expect("Unable to create IrcReactor");
    let server = &PERSISTENT_CONFIG.twitch.connection.server;
    let port = PERSISTENT_CONFIG.twitch.connection.port;
    let ssl = PERSISTENT_CONFIG.twitch.connection.ssl;
    let cfg = IrcConfig {
        server: Some(server.clone()),
        port: Some(port),
        use_ssl: Some(ssl),
        ..IrcConfig::default()
    };

    let client = reactor
        .prepare_client_and_connect(&cfg)
        .expect("Unable to initialize client");

    let (uu, snd) =
        ui::UI::new(client.clone(), channels.iter().next().unwrap().to_owned()).unwrap();
    uu.wait_on_init();

    send!(snd | "# Authenticating...".to_owned())?;
    send!(snd | "## PASS".to_owned())?;

    // Sending auth and nick here. Twitch has a quirk and does not follow the IRCv3
    // spec, so a raw command is needed here Sending OAUTH Code
    let oauth = &PERSISTENT_CONFIG.twitch.oauth;
    client
        .send(Command::Raw("PASS".to_owned(), vec![oauth.clone()], None))
        .expect("Unable to send PASS");

    send!(snd | "## NICK".to_owned())?;
    // Sending User Verification name
    let nick = &PERSISTENT_CONFIG.twitch.username;
    client
        .send(Command::Raw("NICK".to_owned(), vec![nick.clone()], None))
        .expect("Unable to send NICK");

    send!(snd | "# Setting Capabilies".to_owned())?;
    send!(snd | "## membership".to_owned())?;

    // Command caps are pre
    // Sending Membership Capability,
    client
        .send(Command::CAP(
            None,
            CapSubCommand::REQ,
            Some(":twitch.tv/membership".to_owned()),
            None,
        ))
        .expect("Unable to set Membership Capability");

    send!(snd | "## tags".to_owned())?;
    // Sending Channel Capability
    client
        .send(Command::CAP(
            None,
            CapSubCommand::REQ,
            Some(":twitch.tv/tags".to_owned()),
            None,
        ))
        .expect("Unable to set Channel Tag Capabilies");

    // Sending command Capability
    client
        .send(Command::CAP(
            None,
            CapSubCommand::REQ,
            Some(":twitch.tv/commands".to_owned()),
            None,
        ))
        .expect("Unable to set Channel Tag Capabilies");

    send!(snd | "# Jointing all channels...".to_owned())?;
    // Joining all channels
    for chn in channels {
        send!(snd | "## J C {}", chn)?;
        client
            .send(Command::JOIN(chn.clone(), None, None))
            .expect(&format!("Unable to join channel {}", &chn));
    }

    let send = snd.clone();

    reactor.register_client_with_handler(client, move |client, message| {
        use client::Error;
        use irc::error::IrcError;
        let sender = send.clone();
        match client::client_handler_fn(client, message, sender, uu.get_stop()) {
            Ok(o) => Ok(o),
            Err(Error::Irc(e)) => Err(e),
            Err(Error::SendError(e)) => Err(IrcError::from(::std::io::Error::new(
                ::std::io::ErrorKind::Other,
                e,
            ))),
            Err(Error::OperationStopped) => Err(IrcError::from(::std::io::Error::new(
                ::std::io::ErrorKind::Other,
                String::from("Operations Cancelled"),
            ))),
        }
    });

    send!(snd | "# SERVING # SETUP COMPLETE".to_owned())?;

    // Blocking SERVE Call
    if let Err(e) = reactor.run() {
        send!(snd | "E [{:?}] {}", e, e)?;
    }
    Ok(())
}
